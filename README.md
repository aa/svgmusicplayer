Usage
================

Uses d3, installable with:

    npm install d3

Using svgpan

    git clone https://github.com/aleofreddi/svgpan.git

Starting from this directory, create a symbolic link to your music folder.

    ln -s ~/Music .

Generate a playlist file using the find command (via the rule in the makefile):

    make playlist.txt

Use the starter document as a basis for the playlist.svg. This file contains a simple internal stylesheet and a link to the svgpan script.

    cp starter.svg playlist.svg
    
View playlist.html and allow the SVG to be joined with the playlist contents. Click on the audio player (to give the outer frame focus) and press Ctrl-S to attempt to save the SVG (this requires a server that allows file contents to be POSTed such as *makeserver*). When you reload this SVG should be the same as it was before.

Links
============

* [SVG Styling](https://www.w3.org/TR/SVG/styling.html#StyleElementExample)
